package com.hrm.orange;

import com.appium.setup.AndroidAppiumTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class LoginScreenClass extends AndroidAppiumTest {
    loginScreenPageObject loginPageObject;
    @Test
    public void testLoginWithValidInputCredentialsFunction()
    {
        //Set Wait time until Appium server locate elements
        AndroidDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        //Set Email Credentials input
        String userNameInputCredential="Admin";
        //Set Password Credentials input
        String passwordInputCredential="admin123";
        // Initialize LoginPage object
        loginPageObject = new loginScreenPageObject(AndroidDriver);
        //Send Email and password credentials to LoginwithValidCredentials Function
        loginPageObject.loginWithValidInputCredentials(userNameInputCredential,passwordInputCredential);
    }
}
