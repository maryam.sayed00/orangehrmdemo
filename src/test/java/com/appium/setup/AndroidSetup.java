package com.appium.setup;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class AndroidSetup {
    protected AndroidDriver<WebElement> AndroidDriver;

    public  void prepareAndroidForSetUP()  {

        DesiredCapabilities Capability= new DesiredCapabilities();
        Capability.setCapability("platformName","Android");
        Capability.setCapability("platformVersion","8.1.0");
        Capability.setCapability("deviceName","1ed2893e");
        Capability.setCapability("browserName","Chrome");
        Capability.setCapability("noRest", true);
        System.setProperty("webdriver.chrome.driver","C:\\Users\\user\\node_modules\\chromedriver\\lib\\chromedriver\\chromedriver.exe");
        try {
            AndroidDriver=new AndroidDriver<>(new URL("http://0.0.0.0:4723/wd/hub"),Capability);
        } catch (MalformedURLException e) {
            System.out.println("Session not created");
            e.printStackTrace();
        }


    }

}
