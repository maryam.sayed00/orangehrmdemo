package com.hrm.orange;


import io.appium.java_client.AppiumDriver;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class loginScreenPageObject extends loginScreenPageBase {
    public loginScreenPageObject(AppiumDriver<WebElement> AndroidDriver) {
        super(AndroidDriver);
    }
    @FindBy(id="txtUsername")
    WebElement userNameTxtField;
    @FindBy(id="txtPassword")
    WebElement passwordTxtField;
    @FindBy(id="btnLogin")
    WebElement submitButton;

    public void loginWithValidInputCredentials(String userName, String password)
    {
        //navigate to OrangeHRM page
        configDriver.get("https://opensource-demo.orangehrmlive.com");
        //Capturing screenshot before login
        //to make sure that the right screen is opened
        File screenShotBeforeLogin  = ((TakesScreenshot)configDriver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenShotBeforeLogin, new File("C:\\Users\\user\\workspace\\projectname/Screenshots/ScreenshotOfLoginScreenOpen.jpg"));
        } catch (IOException e) {
            System.out.println("Screen can not be captured");
            e.printStackTrace();
        }
// set implicit time wait until object is presented
        configDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        // clear text filed text if exist
        userNameTxtField.clear();
        // set implicit time wait until object is presented
        configDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        // send username input String value
        userNameTxtField.sendKeys(userName);
        // set implicit time wait until object is presented
        configDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        // clear text filed text if exist
        passwordTxtField.clear();
        // set implicit time wait until object is presented
        configDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        // send password input String value
        passwordTxtField.sendKeys(password);
        // set implicit time wait until object is presented
        configDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        //Submit input
        submitButton.click();
        // set implicit time wait until object is presented
        configDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        //capture a screenshot after successful Login
        File screenShotAfterLogin  = ((TakesScreenshot)configDriver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenShotAfterLogin, new File("C:\\Users\\marya\\IdeaProjects\\OrangeHRMSystem/Screenshots/ScreenshotAftersuccessfulLogin.jpg"));
        } catch (IOException e) {
            System.out.println("Screen can not be captured");
            e.printStackTrace();
        }
    }
}
