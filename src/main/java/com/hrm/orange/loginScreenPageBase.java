package com.hrm.orange;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.WebElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class loginScreenPageBase {
    public AppiumDriver<WebElement> configDriver;
    public loginScreenPageBase(AppiumDriver<WebElement> AndroidDriver){
        PageFactory.initElements(new AppiumFieldDecorator(AndroidDriver),this);
        configDriver=AndroidDriver;

    }
}
